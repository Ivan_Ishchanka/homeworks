import argparse
import sys
from connector import db_connect
from typing import Tuple, List

def get_parser() -> argparse.ArgumentParser:
    """Return argument parser

    :return : argparse.ArgumentParser
    """
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-N', "--top_number", type=int)
    arg_parser.add_argument('-genres', "--genres", type=str)
    arg_parser.add_argument('-year_from', "--year_from", type=int)
    arg_parser.add_argument("-year_to", "--year_to", type=int)
    arg_parser.add_argument("-regexp", "--regexp", type=str)
    return arg_parser


def print_top_movies(movies_table: List[Tuple[str, str, int, float]]) -> None:
    """Print movies in csv-like format

    :param movies_table: table with movies
    :return: None
    """
    print('genres,title,year,rating')
    if movies_table:
        for movie in movies_table:
            for genre, title, year, rating in movie:
                print(f'{genre},{title},{year},{rating}')


def get_top_movies(args: argparse.Namespace) -> List[Tuple[str, str, int, float]]:
    """Return list of top movies

    :param args: movie search criteria
    :return: table with movies
    """
    try:
        conn = db_connect()
        cursor = conn.cursor()
        criteria = [args.top_number, args.genres, args.year_from, args.year_to, args.regexp]
        cursor.callproc('sp_find_top_movies', criteria)
        top_movies_table = [row.fetchall() for row in cursor.stored_results()]
        return top_movies_table
    except Exception as e:
        print(e)

    finally:
        conn.close()
        cursor.close()


if __name__ == '__main__':
    args = get_parser().parse_args(sys.argv[1:])
    top_movies_table = get_top_movies(args)
    print_top_movies(top_movies_table)
