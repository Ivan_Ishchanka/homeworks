USE db_movies;

DROP PROCEDURE IF EXISTS `sp_find_top_movies`;
-- DELIMITER //
CREATE PROCEDURE `sp_find_top_movies`(
	IN N 			INT,
    IN genres		VARCHAR(200),
    IN year_from   INT,
    IN year_to		INT,
    IN `regexp`	VARCHAR(200)
)
BEGIN
	WITH TOP_N AS (
		SELECT movies_table.genres, title, `year`, rating, ROW_NUMBER() 
		OVER (
			PARTITION BY movies_table.genres
			ORDER BY rating DESC
		) AS `number` 
		FROM movies_table
		WHERE
			(year_to >= `year` OR year_to IS NULL) AND
			(year_from <= `year` OR year_from IS NULL) AND
            (LOCATE(movies_table.genres, genres) > 0 OR genres IS NULL) AND
            (title REGEXP `regexp` OR `regexp` IS NULL)
	)
	SELECT TOP_N.genres, title, `year`, rating 
	FROM TOP_N WHERE 
		`number` <= N OR N IS NULL;
END -- //
-- DELIMITER 
;