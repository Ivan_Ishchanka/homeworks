USE db_movies;

DROP TABLE IF EXISTS movies_table;
CREATE TABLE movies_table
(
	movie_id 		INT 			NOT NULL,
    title 			VARCHAR(200)	NOT NULL,
    `year`			INT,
    genres 			VARCHAR(150)	NOT NULL,
    rating 			FLOAT,
    CONSTRAINT pk_movies_table PRIMARY KEY (movie_id, genres)
);