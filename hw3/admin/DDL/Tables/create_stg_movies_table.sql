USE db_movies;

DROP TABLE IF EXISTS stg_movies;
CREATE TABLE stg_movies
(
	movie_id 		INT 			NOT NULL,
    title 			VARCHAR(200)	NOT NULL,
    genres 			VARCHAR(150)	NOT NULL,
    CONSTRAINT pk_stg_movies PRIMARY KEY (movie_id)
);
