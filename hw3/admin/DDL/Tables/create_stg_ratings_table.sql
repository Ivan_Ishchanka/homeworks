USE db_movies;

DROP TABLE IF EXISTS stg_ratings;
CREATE TABLE stg_ratings
(
	user_id 		INT 			NOT NULL,
    movie_id 		INT 			NOT NULL,
    rating 			FLOAT 			NOT NULL,
    `timestamp` 	VARCHAR(12),
    CONSTRAINT pk_stg_ratings PRIMARY KEY (user_id, movie_id)
);