USE db_movies;

DROP FUNCTION IF EXISTS `SPLIT_STR`;
CREATE FUNCTION `SPLIT_STR`(
  `string` VARCHAR(200),
  delim VARCHAR(1),
  pos INT
) RETURNS varchar(200) CHARSET utf8mb4
    DETERMINISTIC
RETURN REPLACE(SUBSTRING(SUBSTRING_INDEX(`string`, delim, pos),
       LENGTH(SUBSTRING_INDEX(`string`, delim, pos -1)) + 1),
       delim, ''); 