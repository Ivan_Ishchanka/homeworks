USE db_movies;

DROP FUNCTION IF EXISTS `GET_YEAR`;
CREATE FUNCTION `GET_YEAR`(
	raw_title VARCHAR(200)
) RETURNS int
    DETERMINISTIC
BEGIN
	
	DECLARE year_index INT;
    SET year_index = REGEXP_INSTR (raw_title, '\\(\\d{4}\\)$');
    IF year_index = 0
    THEN
		SET year_index = REGEXP_INSTR (raw_title, '\\(\\d{4}–\\d{4}\\)$');
	ELSE 
		RETURN CAST( SUBSTRING(raw_title, year_index + 1,  4) AS UNSIGNED INTEGER);
	END IF;
    IF year_index = 0
    THEN
		RETURN NULL;
	END IF;
    RETURN CAST( SUBSTRING(raw_title, year_index + 6,  4) AS UNSIGNED INTEGER);
END;