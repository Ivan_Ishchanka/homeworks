USE db_movies;

DROP FUNCTION IF EXISTS `GET_TITLE`;
CREATE FUNCTION `GET_TITLE`(
	raw_title VARCHAR(256)
) RETURNS VARCHAR(256)
    DETERMINISTIC
BEGIN
	DECLARE year_index INT;
    SET year_index = REGEXP_INSTR (raw_title, '\\(\\d{4}\\)$|\\(\\d{4}–\\d{4}\\)$');
	IF year_index = 0
    THEN
		RETURN TRIM(raw_title);
	END IF;
    RETURN TRIM(SUBSTRING(raw_title, 1,  year_index - 1));
END;