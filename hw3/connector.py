import mysql.connector
import configparser


def db_connect():
    """Establish a connection to database using config file"""
    config = configparser.ConfigParser()
    config.read('configs.txt')
    conn = mysql.connector.connect(host=config['db_connect']['host'],
                                   database=config['db_connect']['database'],
                                   user=config['db_connect']['user'],
                                   password=config['db_connect']['password'])
    return conn
