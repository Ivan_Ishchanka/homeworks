from mysql.connector import Error
from connector import db_connect
import csv
import os


def execute_create_script(sql_file_name: str) -> None:
    """Execute scripts of creating tables, functions and procedures

    :param sql_file_name: name of the file with script
    :return: None
    """
    try:
        conn = db_connect()
        cursor = conn.cursor()
        with open(sql_file_name) as sql_file:
            for _ in cursor.execute(sql_file.read(), multi=True):
                pass
        conn.commit()
    except Error as e:
        print(e)

    finally:
        conn.close()
        cursor.close()


def execute_insert_script_template(file_name_with_sql_like_script: str, csv_file_name: str) -> None:
    """Execute script to insert data.

    :param file_name_with_sql_like_script: name of the file with template of sql script
    :param csv_file_name: name of the source csv-file with data
    :return: None
    """
    try:
        conn = db_connect()
        cursor = conn.cursor()
        with open(file_name_with_sql_like_script) as file:
            query = file.read()
        with open(csv_file_name, encoding='UTF-8') as csv_file:
            reader = csv.reader(csv_file, delimiter=',', quotechar='"')
            next(reader)
            for info in reader:
                cursor.execute(query, info)
            conn.commit()
    except Error as e:
        print(e)

    finally:
        conn.close()
        cursor.close()


def execute_procedure(procedure_name: str) -> None:
    try:
        conn = db_connect()
        cursor = conn.cursor()
        cursor.callproc(procedure_name)
        conn.commit()
    except Error as e:
        print(e)

    finally:
        conn.close()
        cursor.close()


def execute_file_in_dir(dir_path: str) -> None:
    for file in os.listdir(dir_path):
        if file.endswith(".sql"):
            execute_create_script(os.path.join(dir_path, file))


if __name__ == '__main__':
    for folder in os.listdir("admin/DDL"):
        execute_file_in_dir(os.path.join("admin/DDL/", folder))
    execute_insert_script_template("admin/DML/insert_movies_template.sql", "data/movies.csv")
    execute_insert_script_template("admin/DML/insert_ratings_template.sql", "data/ratings.csv")
    execute_procedure("sp_prepare_data")
