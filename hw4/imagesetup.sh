git_login=$1

# check for empty login parameter
if [[ -z "$git_login" ]]; then
    echo "Empty login parameter";exit 0;
else
    # cloning repository with homeworks
    git clone https://$git_login@bitbucket.org/coherentprojects/coherent-training-ivan-ischenko.git

    # building an image from a Dockerfile
    docker build . --tag ishchankaimage

    # running bash in a new container
    docker run -it ishchankaimage bash
fi