import sys
import converter
import argparse


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-to_csv', "--to_csv", action="store_true")
    arg_parser.add_argument('-to_parquet', "--to_parquet", action="store_true")
    arg_parser.add_argument('-schema', "--schema", action="store_true")
    arg_parser.add_argument("-i", "--input_file_name", type=str, required=True)
    arg_parser.add_argument("-o", "--output_file_name", type=str, default='')
    args = arg_parser.parse_args(sys.argv[1:])

    if args.to_parquet:
        converter.csv_to_parquet(args.input_file_name, args.output_file_name)
    elif args.to_csv:
        converter.parquet_to_csv(args.input_file_name, args.output_file_name)
    elif args.schema:
        schema = converter.get_parquet_schema(args.input_file_name)
        if schema is not None:
            print(schema)
