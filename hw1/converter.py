import pyarrow.parquet as parquet
import pandas


def csv_to_parquet(input_file_name: str, output_file_name: str) -> None:
    """Convert from csv to parquet.

    Keyword arguments:
    input_file_name -- the name of csv file.
    output_file_name -- the name of the file to be returned.

    """
    try:
        data_frame = pandas.read_csv(input_file_name)
        data_frame.to_parquet(output_file_name)
    except Exception as e:
        print('Error. {}'.format(e))


def parquet_to_csv(input_file_name: str, output_file_name: str) -> None:
    """Convert parquet to csv.

    Keyword arguments:
    input_file_name -- the name of csv file.
    output_file_name -- the name of the file to be returned.

    """
    try:
        data_frame = pandas.read_parquet(input_file_name)
        data_frame.to_csv(output_file_name, index=False)
    except Exception as e:
        print('Error. {}'.format(e))


def get_parquet_schema(file_name: str) -> parquet.ParquetFile.schema:
    """Return parquet schema of the file.

    Keyword arguments:
    file_name -- the name of parquet file.

    """
    try:
        parquet_file = parquet.ParquetFile(file_name)
        return parquet_file.schema
    except Exception as e:
        print('Error. {}'.format(e))
