MAPPER_ARGUMENTS=""
TOP_NUMBER=0

while [ "$1" ]
do
    case "$1" in
    -N| --top_number) TOP_NUMBER="$1 $2 " ;;
    --genres| --regexp| --year_from| --year_to) MAPPER_ARGUMENTS+="$1 $2 " ;;
    esac
    shift
done

if [ "${TOP_NUMBER}" == 0 ]; then
    if [ "${MAPPER_ARGUMENTS}" == "" ]; then
        cat data/ml-latest-small/movies.csv|python3 mapper.py|sort|python3 reducer.py
    else
        cat data/ml-latest-small/movies.csv|python3 mapper.py ${MAPPER_ARGUMENTS}|sort|python3 reducer.py
    fi
else
    if [ "${MAPPER_ARGUMENTS}" == "" ]; then
        cat data/ml-latest-small/movies.csv|python3 mapper.py|sort|python3 reducer.py ${TOP_NUMBER}
    else
        cat data/ml-latest-small/movies.csv|python3 mapper.py ${MAPPER_ARGUMENTS}|sort|python3 reducer.py ${TOP_NUMBER}
    fi
fi