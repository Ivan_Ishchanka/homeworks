MAPPER_STR="python3.6 mapper.py "
REDUCER_STR="python3.6 reducer.py "

while [ "$1" ]
do
    case "$1" in
    -N| --top_number) REDUCER_STR+="$1 $2 ";;
    --genres| --regexp| --year_from| --year_to) MAPPER_STR+="$1 $2 " ;;
    esac
    shift
done

hdfs dfs -mkdir data
hdfs dfs -rm -r -skipTrash output
hdfs dfs -put coherent-training-ivan-ischenko/hw5/hdfs/data/ml-latest-small/movies.csv data

hadoop jar /usr/hdp/3.*/hadoop-mapreduce/hadoop-streaming.jar \
-input data/movies.csv -output output \
-file ~/coherent-training-ivan-ischenko/hw5/mapper.py -mapper "${MAPPER_STR}" \
-file ~/coherent-training-ivan-ischenko/hw5/reducer.py -reducer "${REDUCER_STR}"

hdfs dfs -cat output/part-00000