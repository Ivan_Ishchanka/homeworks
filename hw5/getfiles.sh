wget --no-check-certificate https://files.grouplens.org/datasets/movielens/ml-latest-small.zip
wget --no-check-certificate https://files.grouplens.org/datasets/movielens/ml-latest.zip
mkdir data
unzip ml-latest-small.zip -d data
unzip ml-latest.zip -d data