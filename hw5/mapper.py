import argparse
import csv
import sys
import re
from typing import List, Tuple


def get_parser() -> argparse.ArgumentParser:
    """Return argument parser

    :return : argparse.ArgumentParser
    """
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("--genres", type=str)
    arg_parser.add_argument("--year_from", type=int)
    arg_parser.add_argument("--year_to", type=int)
    arg_parser.add_argument("--regexp", type=str)
    return arg_parser


def get_parsed_movie_name_and_year(name_with_year: str) -> Tuple[str, int]:
    """Separate name from year

    :param name_with_year: string, that contains name and year
    :return: list, that contains name and year
    """
    try:
        open_index = name_with_year.rindex('(')
        close_index = name_with_year.rindex(')')
        name = name_with_year[:open_index].strip()
        year = name_with_year[open_index + 1:close_index]
        if '-' in year:
            year = year.split('–')[1]
        return name, int(year)
    except Exception:
        return name_with_year, None


def get_parsed_movie_genres(str_with_genres: str) -> List[str]:
    """Separate genres from each other

    :param str_with_genres: genres in string, separated by '|'
    :return: list with genres
    """
    if '|' in str_with_genres:
        genres = list()
        for genre in str_with_genres.split('|'):
            genres.append(genre.rstrip().lstrip())
        return genres
    else:
        return [str_with_genres.rstrip().lstrip()]


def map(line_number: int, line: str):
    movie_id, title_with_year, movie_genres = list(csv.reader([line], delimiter=','))[0]
    title, year = get_parsed_movie_name_and_year(title_with_year)
    genres = get_parsed_movie_genres(movie_genres)
    if not ((args.year_from and (not year or year < args.year_from)) or
            (args.year_to and (not year or year > args.year_to)) or
            (args.regexp and not re.match(args.regexp, title))):
        if args.genres:
            for genre in genres:
                if genre in args.genres:
                    yield genre, {"title": title, "year": year}
        else:
            for genre in genres:
                yield genre, {"title": title, "year": year}


args = None

if __name__ == '__main__':
    args = get_parser().parse_args(sys.argv[1:])
    next(sys.stdin)
    line_number = 0
    for line in sys.stdin:
        for key, value in map(line_number, line):
            print(f"{key}\t{value}")
        line_number += 1
