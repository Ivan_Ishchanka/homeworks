import sys
from typing import Dict, List, Tuple
import ast
import argparse


def get_parser() -> argparse.ArgumentParser:
    """Return argument parser

    :return : argparse.ArgumentParser
    """
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-N', "--top_number", type=int)
    return arg_parser


def get_genre_and_title_with_year(genre_with_other_movie_info: str) -> Tuple[str, Tuple[str, int]]:
    """Separate genre from other movie information

    :param genre_with_other_movie_info: string, that contains name and other movie info
    :return: tuple, that contains genre and tuple with movie_id, title and year
    """
    genre, title_and_year = genre_with_other_movie_info.split('\t')
    title_and_year = ast.literal_eval(title_and_year)
    return genre, title_and_year


def print_in_json_format(key: str, values: List) -> Dict[str, List[Dict]]:
    """Print movies in json format

    :param key: genre
    :param values: List of movies
    :return: Dictionary with genre and movies information corresponding to this genre
    """
    print({"genre": key, "movies": values})


def movies_cmp_by_year(movie: Dict[str, int]) -> int:
    """Comparator for sorting movies by year"""
    if not movie['year']:
        return 0
    return movie['year']


def reduce(key: str, values: List) -> Tuple[str, List]:
    return key, sorted(sorted(values, key=lambda value: value['title']), key=movies_cmp_by_year, reverse=True)[
                :args.top_number]


args = None

if __name__ == '__main__':
    args = get_parser().parse_args(sys.argv[1:])
    prev_key = None
    values = []
    for line in sys.stdin:
        key, value = get_genre_and_title_with_year(line)
        if key != prev_key and prev_key:
            result_key, result_value = reduce(prev_key, values)
            print_in_json_format(result_key, result_value)
            values = []
        prev_key = key
        values.append(value)
    if prev_key:
        result_key, result_value = reduce(prev_key, values)
        print_in_json_format(result_key, result_value)
