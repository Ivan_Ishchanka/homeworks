import argparse
import sys
import movies_dataset


def get_parser() -> argparse.ArgumentParser:
    """Return argument parser

    :return : argparse.ArgumentParser
    """
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-N', "--top_number", type=int)
    arg_parser.add_argument('-genres', "--genres", type=str)
    arg_parser.add_argument('-year_from', "--year_from", type=int)
    arg_parser.add_argument("-year_to", "--year_to", type=int)
    arg_parser.add_argument("-regexp", "--regexp", type=str)
    return arg_parser


def print_top_movies_table(movies_table):
    print('genres,title,year,rating')
    for _, title, year, genres, rating in movies_table.get_sorted_table(args.top_number):
        print(f'{genres},{title},{year},{rating}')


if __name__ == '__main__':
    args = get_parser().parse_args(sys.argv[1:])
    movies_table = movies_dataset.MoviesDataset(movies_file_name='data/movies.csv', rating_file_name='data/ratings.csv',
                                                args=args)
    print_top_movies_table(movies_table)
