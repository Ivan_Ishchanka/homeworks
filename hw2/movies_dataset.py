import argparse
import csv
import re
import operator
from typing import Dict, List, Tuple


class MoviesDataset:

    def __init__(self, movies_file_name: str, rating_file_name: str, args: argparse.Namespace):
        """Initialize movies dataset with movies_dictionary and ratings_dictionary

        :param movies_file_name: name of the file with movies info
        :param rating_file_name: name of the file with ratings info
        :param args: arguments for movies filtering
        """

        self.total_genres = list()
        self.movies_dictionary = self.create_movies_dictionary(movies_file_name, args)
        self.ratings_dictionary = self.create_ratings_dictionary(rating_file_name)
        self.add_rating_to_movies()

    def create_movies_dictionary(self, movies_file_name: str, args: argparse.Namespace) -> Dict[str, Tuple[str, int, List[str], float]]:
        """Create movies dictionary with movie id, title, year, rating

        :param movies_file_name: name of the file with movies info
        :param args: arguments for movies filtering
        :return: dictionary with movies
        """
        movies_dictionary = dict()
        if args.genres:
            self.total_genres = args.genres.split('|')
        with open(movies_file_name, encoding='UTF-8') as movies_file:
            movies_reader = csv.reader(movies_file, delimiter=',', quotechar='"')
            next(movies_reader)
            for movie_id, movie_title, movie_genres in movies_reader:
                name, year = self.get_parsed_movie_name_and_year(movie_title)
                genres = self.get_parsed_movie_genres(movie_genres)
                if args.genres:
                    needed_genre = False
                    for genre in genres:
                        if genre in args.genres:
                            needed_genre = True
                            break
                    if not needed_genre:
                        continue
                else:
                    for genre in genres:
                        if genre not in self.total_genres:
                            self.total_genres.append(genre)
                if (args.year_from and (not year or year < args.year_from)) or \
                        (args.year_to and (not year or year > args.year_to)) or \
                        (args.regexp and not re.match(args.regexp, name)):
                    continue
                movies_dictionary[movie_id] = [name, year, genres, 0]
        return movies_dictionary

    def create_ratings_dictionary(self, rating_file_name: str) -> Dict[str, Tuple[float, int]]:
        """Create rating dictionary with movie id, total points, number of reviews

        :param rating_file_name: name of the file with ratings info
        :return: dictionary with records
        """
        ratings_dictionary = dict()
        with open(rating_file_name) as ratings_file:
            rating_reader = csv.reader(ratings_file)
            next(rating_reader)
            for _, movie_id, rating, _ in rating_reader:
                if movie_id in ratings_dictionary:
                    ratings_dictionary[movie_id][0] += float(rating)
                    ratings_dictionary[movie_id][1] += 1
                else:
                    ratings_dictionary[movie_id] = [float(rating), 1]
        return ratings_dictionary

    def add_rating_to_movies(self) -> None:
        """Add ratings to movies in movies dictionary

        :return: None
        """
        for movie_id in self.ratings_dictionary:
            if movie_id in self.movies_dictionary:
                total_points, number_of_reviews = self.ratings_dictionary[movie_id]
                movie_rating = total_points / number_of_reviews
                self.movies_dictionary[movie_id][3] = movie_rating

    def get_parsed_movie_name_and_year(self, name_with_year: str) -> Tuple[str, int]:
        """Separate name from year

        :param name_with_year: string, that contains name and year
        :return: list, that contains name and year
        """
        try:
            open_index = name_with_year.rindex('(')
            close_index = name_with_year.rindex(')')
            name = name_with_year[:open_index].strip()
            year = name_with_year[open_index + 1:close_index]
            if '-' in year:
                year = year.split('–')[1]
            return name, int(year)
        except Exception:
            return name_with_year, None

    def get_parsed_movie_genres(self, str_with_genres: str) -> List[str]:
        """Separate genres from each other

        :param str_with_genres: genres in string, separated by '|'
        :return: list with genres
        """
        if '|' in str_with_genres:
            return str_with_genres.split('|')
        else:
            return [str_with_genres]

    def get_sorted_table(self, top_number) -> Tuple[int, str, int, List[str], float]:
        """Sort table by rating

        :param top_number: count of records in sorted table
        :return: Top N records from table
        """
        sorted_table = list()
        for sort_genre in self.total_genres:
            tmp_list = list()
            for movie_id in self.movies_dictionary:
                name, year, genre, rating = self.movies_dictionary[movie_id]
                if sort_genre in genre:
                    tmp_list.append([movie_id, name, year, sort_genre, rating])
            tmp_list.sort(key=operator.itemgetter(4), reverse=True)
            if not top_number:
                sorted_table += tmp_list
            else:
                sorted_table += tmp_list[:top_number]
        return sorted_table