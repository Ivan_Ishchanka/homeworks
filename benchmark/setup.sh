sudo apt-get update --quiet && \
sudo apt-get install python3 --yes --quiet && \
sudo apt-get install python3-pip --yes --quiet

sudo apt-get install mysql-server --yes --quiet

git clone https://bitbucket.org/coherentprojects/coherent-training-ivan-ischenko/src/master/

pip3 install -r master/hw1/requirements.txt
pip3 install -r master/hw3/requirements.txt
pip3 install cmdbench

sudo service mysql start && \
sudo mysql -e "ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '6996'; CREATE DATABASE IF NOT EXISTS db_movies;"